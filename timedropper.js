/**
 * @file
 * Javascript file for the Timedropper module.
 */

(function ($) {
  'use strict';

  Drupal.behaviors.timedropper = {
    attach: function (context, settings) {
      $('.timedropper', context).each(function () {
        var id = $(this).attr('data-timedropper');
        var config = [];
        if (typeof settings.timedropper.instances[id] !== 'undefined') {
          config = settings.timedropper.instances[id];
        }
        $(this, context).timeDropper(config);
      });
    }
  };
}(jQuery));
