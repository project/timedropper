CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Timedropper module integrates the Timedropper jQuery plugin into Drupal
which provides a fancy timepicker.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/timedropper

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/search/timedropper


REQUIREMENTS
------------

This module requires the following modules:

 * Date API (https://drupal.org/project/date)
 * Date Popup (https://drupal.org/project/date)
 * Libraries API (https://drupal.org/project/libraries)

The Timedropper plugin also requires atleast jQuery version 1.7 which requires
either one of the following modules:

 * jQuery Update (https://drupal.org/project/jquery_update)
 * jQuery Multi (https://drupal.org/project/jqmulti)

This module requires the following plugin:

 * Timedropper jQuery plugin: view the installation part of this file for more
   information.


INSTALLATION
------------

 * Module: Install as you would normally install a contributed Drupal module.
   See: https://drupal.org/documentation/install/modules-themes/modules-7 for
   further information.

 * Plugin: Download the most recent version of the Timedropper jQuery plugin
   from http://felicegattuso.com/projects/timedropper. Extract the contents
   into the sites/all/libraries/timedropper directory.


CONFIGURATION
-------------
 * Enable the Timedropper plugin via Configuration » Date API » Date Popup

 * Configure Timedropper for a field by going to the field configuration via
   the field UI.


MAINTAINERS
-----------

Current maintainers:
 * Mitch Portier (Arkener) - https://drupal.org/user/2284182
